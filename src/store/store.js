// modules
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

// reducer
import rootReducer from '../reducers/root.js';

export default function configureStore() {
    return createStore(
        rootReducer,
        applyMiddleware(thunk)
    );
}