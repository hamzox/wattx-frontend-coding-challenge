// actions
import { COIN_ACTIONS, LIMIT_SELECTOR_ACTIONS } from "../constants/actionTypes";

// initial state of gallery component
const initialState = { 
    limit: "10",
    coins: [],
    isCoinsLoading: false,
};

export default (state = initialState, action) => {
    switch (action.type) {
        // Ticker Actions
        case COIN_ACTIONS.GET_TICKER_REQUEST:
            return {
                ...state,
                isCoinsLoading: true,
            }

        case COIN_ACTIONS.GET_TICKER_SUCCESS:
            return {
                ...state,
                coins: action.payload.data,
                isCoinsLoading: false,
            }

        case COIN_ACTIONS.GET_TICKER_FAILURE:
            return {
                ...state,
                isCoinsLoading: false,
            }

        // Limit Selector Actions
        case LIMIT_SELECTOR_ACTIONS.UPDATE_LIMIT_VALUE:
            return {
                ...state,
                limit: action.payload
            }
        default:
            return state
    }
}