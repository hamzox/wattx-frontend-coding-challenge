// modules
import { combineReducers } from 'redux';

// reducers
import market from './market.js';

export default combineReducers({
    market
});