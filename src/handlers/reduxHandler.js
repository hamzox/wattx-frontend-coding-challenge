/**
 * Helper method for dispacting events and associated payloads 
 * to redux store.
 */
export const actionPayload = (type, payload) => {
    return { type, payload }
}