// actions
import { COIN_ACTIONS, LIMIT_SELECTOR_ACTIONS } from "../constants/actionTypes";

// services & handlers
import coinService from "../services/coinService";
import { actionPayload } from "../handlers/reduxHandler";

 /**
  * Load up all cryptocurrency data.  This data is used to find what logos
  * each currency has, so we can display things in a friendly way.
  */
export const getCoins = (params) => {
  return function(dispatch) {
    dispatch(actionPayload(COIN_ACTIONS.GET_TICKER_REQUEST))
    coinService.getCoins(params).then(response => {
      dispatch(actionPayload(COIN_ACTIONS.GET_TICKER_SUCCESS, response.data))
    }).catch(error => {
      dispatch(actionPayload(COIN_ACTIONS.GET_TICKER_FAILURE, error))
    }) 
  }
}

/**
  * Update Limit Selector value to redux store
  * later other pages can take advantage of it's value.
  */
export const updateLimit = (limit) => {
  return function(dispatch) {
    dispatch(actionPayload(LIMIT_SELECTOR_ACTIONS.UPDATE_LIMIT_VALUE, limit))
  }
}