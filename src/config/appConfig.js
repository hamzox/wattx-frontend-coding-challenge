export const BuildMode = {
    BUILD_MODE_LOCAL: 'local',
    BUILD_MODE_DEV: 'dev',
    BUILD_MODE_QA: 'qa',
    BUILD_MODE_STAGING: 'staging',
    BUILD_MODE_PROD: 'prod',
    BUILD_MODE_DEMO: 'demo',
  };
  
  export const Environment = {
  
    /* Configure this option before build relaese: */
    /* >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> */
  
    buildMode: BuildMode.BUILD_MODE_LOCAL,
  
    /* >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> */
  
    allConfigurations: {
      local: {
        baseUrl: '',
        apiKey: '',
      },
      dev: {
        baseUrl: '',
        apiKey: '',
      },
      qa: {
        baseUrl: '',
        apiKey: '',
      },
      staging: {
        baseUrl: '',
        apiKey: '',
      },
      prod: {
        baseUrl: '',
        apiKey: '',
      },
    },
  
    config(buildMode) {
      const mode = buildMode || this.buildMode;
      return this.allConfigurations[mode];
    },
  };
  