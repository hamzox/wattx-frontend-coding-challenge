// modules
import axios from 'axios';

// config
import { Environment } from '../config/appConfig';

/**
 * @about This file act as a wrapper for axios client
 * Additional methods and options can be added according to requirements
 */

export default () => {
  const { baseUrl } = Environment.config();
  const defaultOptions = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  return {
    get: (url, options = {}) => axios.get(`${baseUrl}${url}`, { ...defaultOptions, ...options }),
    post: (url, data, options = {}) => axios.post(`${baseUrl}${url}`, data, { ...defaultOptions, ...options }),
    put: (url, data, options = {}) => axios.put(`${baseUrl}${url}`, data, { ...defaultOptions, ...options }),
    delete: (url, options = {}) => axios.delete(`${baseUrl}${url}`, { ...defaultOptions, ...options }),
  };
};
