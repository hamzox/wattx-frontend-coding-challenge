// services & helpers
import apiClient from './apiClient';

// constants
import appConstants from '../constants/appConstants';

export default class {
    /**
     * @param {Object} params with `limit`, `start` & `convert` keys
     * @about uses `apiClient` to get data from endpoint
     */
    static async getCoins(params = {}) {
        const base = appConstants.CoinMarketApiBase;
        const defaultParams = appConstants.CoinDefaultParams;
        let url = base+`ticker?start=${params.start || defaultParams.START}&limit=${params.limit || defaultParams.LIMIT}&convert=${params.convert || defaultParams.CONVERT}`;
        return await apiClient().get(url);
    }

    /**
     * @param {String | Number} currency
     * @about format currency with commas. e.g. 100000 -> 100,000
     */
    static formatCurrency(currency) {
        return currency.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    /**
     * @param {Array} coins
     * @about formats coins array to UI specific objects and related null checks
     */
    static formatCoinsForChart(coins) {
        return coins.map(
            (coin) => ({
                name: coin.name,
                symbol: coin.symbol,
                market_cap: coin.quote.USD.market_cap ? parseFloat((coin.quote.USD.market_cap / 1000000).toFixed(2)) : 0,
                volume: coin.quote.USD.volume_24h ? parseFloat((coin.quote.USD.volume_24h / 1000000).toFixed(2)) : 0,
                price_change: coin.quote.USD.percent_change_24h ? coin.quote.USD.percent_change_24h.toFixed(2) : 0
            })
        );
    }

    /**
     * @param {Array} coins
     * @about returns `cols[]` and `rows[]` for `MarketTable` component
     */
    static getMarketTableData(coins) {
        const cols = [{
            id: "col-1",
            value: "Rank"
        },{
            id: "col-2",
            value: "Rank"
        },{
            id: "col-3",
            value: "Price Change (24h)"
        },{
            id: "col-4",
            value: "Market Cap"
        },{
            id: "col-5",
            value: "Price Change (24h)"
        },{
            id: "col-6",
            value: "Volume (24h)"
        }]
        const rows = coins.map(coin => {
            return {
                id: coin.id,
                rank: coin.cmc_rank || "-",
                name: coin.name || "-",
                price: coin.quote.USD.price || 0,
                marketCap: coin.quote.USD.market_cap || 0,
                priceChange24H: coin.quote.USD.percent_change_24h || 0,
                volum24H: coin.quote.USD.volume_24h || 0,
            }
        });
        return {
            rows,
            cols,
        }
    }
}