// modules
import React from 'react';
import 'semantic-ui-css/semantic.min.css';

// components
import Navigation from './components/Navigation';
import LimitSelector from './components/LimitSelector';

// styles
import './App.css';

function App() {
  return (
    <div className="App">
      <LimitSelector />
      <Navigation />
    </div>
  );
}

export default App;
