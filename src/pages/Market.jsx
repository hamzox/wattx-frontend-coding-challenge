// modules
import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Loader } from 'semantic-ui-react'

// components
import MarketTable from '../components/MarketTable';

// actions, services
import { getCoins } from '../actions/market';
import coinService from '../services/coinService';

class Market extends React.Component {

  componentDidMount() {
    this.getCoins();
  }

  /**
   * Method to get coins and update the store via `getCoins` action.
   */
  getCoins() {
    const { limit, coins } = this.props;
    if (!coins.length) {
      const params = {
        limit,
      }
      this.props.getCoins(params);
    }
  }

  render() {
    const { coins, isCoinsLoading } = this.props;
    const tableData = coinService.getMarketTableData(coins);
    return (
      <>
        <div className={isCoinsLoading ? 'c-disabled-section' : ''}>
          {coins.length > 0 && <MarketTable {...tableData} />}
        </div>
        <Loader active={isCoinsLoading}/> 
      </>
    ); 
  }
}

// Redux store data available in market props
function mapStateToProps(state) {
  const { coins, limit, isCoinsLoading } = state.market
  return {
    coins,
    limit,
    isCoinsLoading
  };
}

// Actions available in market props to update Redux store data.
function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      getCoins
    }, dispatch,
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(Market);
