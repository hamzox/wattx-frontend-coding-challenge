// modules
import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Loader } from 'semantic-ui-react'

// components
import LiquidityChart from '../components/liquidity/LiquidityChart';
import coinService from '../services/coinService';

// actions & services
import { getCoins } from '../actions/market';

class Liquidity extends React.Component {

  componentDidMount() {
    this.getCoins();
  }

  /**
   * Method to get coins and update the store via `getCoins` action.
   * This method is necessary, if user directly lands to liquidity route
   */
  getCoins() {
    const { limit, coins } = this.props;
    if (!coins.length) {
      const params = {
        limit
      }
      this.props.getCoins(params);
    }
  }

  render() {
    const { coins, isCoinsLoading } = this.props
    const chartData = coinService.formatCoinsForChart(coins);
    return (
      <>
        <div className={isCoinsLoading ? 'c-disabled-section' : ''}>
          <LiquidityChart data={chartData}/>
        </div>
        <Loader active={isCoinsLoading}/> 
      </>
    );
  }
}

// Redux store data available in props
function mapStateToProps(state) {
  const { coins, limit, isCoinsLoading } = state.market
  return {
    limit,
    coins,
    isCoinsLoading
  };
}

// Actions available in props to update Redux store data.
function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      getCoins
    }, dispatch,
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(Liquidity);
