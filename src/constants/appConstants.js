export default {
    CoinMarketApiBase: "http://localhost:3009/api/v1/",
    CoinDefaultParams: {
        LIMIT: "10",
        START: "1",
        CONVERT: "USD"
    }
}