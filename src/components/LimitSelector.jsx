// modules
import React, { Component } from 'react'
import {
  Form,
  Select,
} from 'semantic-ui-react'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

// actions
import { getCoins, updateLimit } from '../actions/market';

/**
 * Component to hange of limit of coins API globally,
 * Uses `semantic-ui` dropdown and associated handler methods.
 */

class LimitSelector extends Component {

    constructor(props) {
        super(props);
        this.state = {
            options: [
                { key: 'limit-10', text: '10', value: '10' },
                { key: 'limit-50', text: '50', value: '50' },
                { key: 'limit-all', text: 'All', value: '5000' }, // 5000. why? - max limit on API documentatation
            ]
        }
    }

    ///// Handlers

    /**
     * This method is called on change of global limit selector dropdown,
     * it uses additional `updateLimit` & `getCoins` method to update the store globally.
     */

    onLimit = (name, formField) => {
        const { limit } = this.props;
        const newValue = formField.value
        if (newValue !== limit) {
            const params = {
                limit: newValue
            }
            this.props.updateLimit(newValue);
            this.props.getCoins(params);
        }
    }
    
    render() {
        const { options } = this.state;
        const { limit } = this.props;
        return (
            <div className="c-limit-selector">
                <Form.Group widths='equal'>
                    <Form.Field
                        onChange={this.onLimit}
                        defaultValue={limit}
                        control={Select}
                        options={options}
                        placeholder='Limit'
                    />
                </Form.Group>
            </div>
        )
    }
}

// Redux store data available in props
function mapStateToProps(state) {
    const { limit } = state.market
    return {
      limit,
    };
}
  
// Actions available in props to update Redux store data.
function mapDispatchToProps(dispatch) {
    return bindActionCreators(
      {
        getCoins,
        updateLimit
      }, dispatch,
    );
}
  
export default connect(mapStateToProps, mapDispatchToProps)(LimitSelector);
