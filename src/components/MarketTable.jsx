// modules
import React from 'react'
import { Table } from 'semantic-ui-react'

// helpers & services
import coinService from '../services/coinService';


/**
 * Table component for Market page.
 * Stateless component, as it doesn't require any interaction (as per requirements).
 * Uses `rows[]` & `cols[]` as props.
 */

const MarketTable = props => {
    const { rows = [], cols = [] } = props;
    return (
        <Table className="c-market-table" celled>
            <Table.Header>
                <Table.Row>
                    {
                        cols.map(col => {
                            return (<Table.HeaderCell key={col.id}>{col.value}</Table.HeaderCell>)
                        })
                    }
                </Table.Row>
            </Table.Header>
            <Table.Body>
                {
                    rows.map(row => {
                        return (
                            <Table.Row key={row.id}>
                                <Table.Cell>{row.rank}</Table.Cell>
                                <Table.Cell>{row.name}</Table.Cell>
                                <Table.Cell>${coinService.formatCurrency(row.price)}</Table.Cell>
                                <Table.Cell>${coinService.formatCurrency(row.marketCap)}</Table.Cell>
                                <Table.Cell>{row.priceChange24H.toFixed(2)}%</Table.Cell>
                                <Table.Cell>${coinService.formatCurrency(row.volum24H)}</Table.Cell>
                            </Table.Row>
                        )
                    })
                }
            </Table.Body>
        </Table>
    );
}

export default MarketTable