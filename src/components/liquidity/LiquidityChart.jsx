// modules
import React, { Component } from 'react';
import {
    Label,
    ResponsiveContainer,
    Scatter,
    ScatterChart,
    Tooltip,
    XAxis,
    YAxis,
    ZAxis,
} from 'recharts';

// components
import LiquidityChartTooltip from './LiquidityChartTooltip';

/**
  * Liquidity chart, using `recharts` react library
  * as it satisfies the requirements in very easy way.
  */
class LiquidityChart extends Component {
    render() {
        const { data } = this.props;
        return (
            <ResponsiveContainer width="80%" height={500}>
                <ScatterChart width={730} height={250} margin={{
                        top: 10, right: 5, bottom: 20, left: 30
                    }}>
                    <XAxis
                        dataKey="market_cap"
                        minTickGap={10}
                        name="Market Capitalization"
                        unit="MM">
                        <Label value="Market Capitalization" offset={-10} position="insideBottom" />
                    </XAxis>
                    <YAxis dataKey="volume" name="Volume (24h)" unit="MM" />
                    <ZAxis dataKey="price_change" range={[0, 100]} name="Price Change(24h)" unit="%" />
                    <Tooltip
                        cursor={{ 
                            strokeDasharray: '3 3' 
                        }} 
                        content={<LiquidityChartTooltip />}
                        wrapperStyle={{ 
                            backgroundColor: 'rgba(1,1,1,0.05)', padding:'10px'
                        }}
                    />
                    <Scatter name="Currencies" data={data} fill="#8884d8" />
                </ScatterChart>
            </ResponsiveContainer>
        )
    }
}

export default LiquidityChart;