import React from 'react';

/**
  * Liquidity chart tooltip, passed as a custom `content` parameter
  * to `ToolTip` component provided by `recharts`.
  */

const LiquidityChartTooltip = ({ active, payload }) => {
    if (active && payload.length) {
        const name = payload[0].payload.name;
        const symbol = payload[0].payload.symbol;
        const market_cap = payload[0].payload.market_cap.toLocaleString();
        const volume = payload[0].payload.volume.toLocaleString();
        const price_change = payload[0].payload.price_change;
        return (
            <div className="c-liquidity-chart-tooltip">
                <p>{`${name} : ${symbol}`}</p>
                <p>{`Market Capitalization : $${market_cap} MM`}</p>
                <p>{`Volume(24h) : $${volume} MM`}</p>
                <p>{`Price Change(24h) : ${price_change}%`}</p>
            </div>
        )
    }
    return null
}

export default LiquidityChartTooltip;
