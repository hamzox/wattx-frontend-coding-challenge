// modules
import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

// pages
import Market from '../pages/Market';
import Liquidity from '../pages/Liquidity';

/**
 * Component for navigating between Market and Liquidity page.
 * Uses `react-router-dom`
 */

function Navigation() {
    return (
        <div className="c-navigation">
            <Router>
                <div>
                    <ul>
                        <li>
                            <Link to="/">Market</Link>
                        </li>
                        <li>
                            <Link to="/liquidity">Liquidity</Link>
                        </li>
                    </ul>
                    <Switch>
                        <Route exact path="/">
                            <Market />
                        </Route>
                        <Route path="/liquidity">
                            <Liquidity />
                        </Route>
                    </Switch>
                </div>
            </Router>
        </div>
    )
}

export default Navigation;