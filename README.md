Wattx Frontend Engineer Coding Challenge

## Installation and Getting Started

Installing the project is pretty standard with Node (`npm`). So, right after cloning the project, install the dependencies by running;

`>> npm install`

### Available Scripts (Frontend/Backend)

Run Frontend using: 

`>> npm start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser. (The page will reload if you make edits)

Run Server using:

`>> npm run server`

Run the server on port `3009`
For now, the API key is hard coded to server's index file, however it can be fetched from environment variables.

If you want to test app with your own key, you can get if from [here](https://coinmarketcap.com/api/).

### About the API

After doing some research I came to know that `ticker/` endpoint was not available as it was depreciated, but I found the equivalent, i.e. `/v1/cryptocurrency/listings/latest`. However, even after integration, I noticed my requests weren't getting through the preflight check on CMC's API as it's written on the documentation:

>Note: Making HTTP requests on the client side with Javascript is currently prohibited through CORS configuration. This is to protect your API Key which should not be visible to users of your application so your API Key is not stolen. Secure your API Key by routing calls through your own backend service.

To resolve this, I created my own `server` folder at the root of the project, to proxy the API. So this means we need both frontend and backend running, in order to make the app work.

### Technology Stack

* React.js
* Redux
* Express.js
* Semantic UI
* Recharts (React Chart Library)

### GIT Strategy

Demonstration of GIT Flow model with clean commit history, with master as `production` branch, `dev` for development. Later branches can be made from `dev` as `feature/TICKET_NUMBER-XXX` or `bug/TICKET_NUMBER-XXX` or maybe from `production` for hotfixes.

### Important Notes
Tests and stylings are not covered in this project due to time limits and was not the part of requirement.

### References

* [Create React App](https://facebook.github.io/create-react-app/docs/getting-started)
* [Semantic UI React](https://react.semantic-ui.com/)
* [Recharts](http://recharts.org/en-US/)

### References
Hamza Khan - Frontend Engineer