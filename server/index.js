/**
 * @about Temporary Node server uses `express` as server side framework to
 * target Coin Market Cap APIs due to CORS issue as it cannot be called directly from Frontend
 */

const express = require('express');
const bodyParser = require('body-parser');
const pino = require('express-pino-logger')();
const request = require('request');
const cors = require('cors');

////// Initializations //////

const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(pino);
app.use(cors());

const apiKey = "9be820b8-e573-4717-990d-09a33ce3ded8";
const baseUrl = 'https://pro-api.coinmarketcap.com/v1/';
const customTickerEndpoint = 'cryptocurrency/listings/latest';

app.get('/api/v1/ticker', (req, res, next) => {
  const { start, limit, convert } = req.query;
  const options = {
      uri: baseUrl+customTickerEndpoint+`?start=${start}&limit=${limit}&convert=${convert}`,
      method: 'GET',
      headers: {
        "X-CMC_PRO_API_KEY": apiKey,
      }
  };
  request(options, (error, response, body) => {
    res.setHeader('Content-Type', 'application/json');
    res.send(body);
  });
});

app.listen(3009, () =>
  console.log('Express server is running on localhost:3009 ')
);